<?php
/**
 * @file
 * Template for foundation page.
 */
?>
<!doctype html>
<html class='no-js' lang='en'>
<head>
  <meta charset='utf-8' />
  <meta name='viewport' content='width=device-width' />
  <link rel='stylesheet' href='css/style.css' />
  
  <title></title>
</head>
<body>
  <?php print drupal_render($ds_content); ?>
  <script type='text/javascript' type='text/javascript' src='js/script.js'></script>
</body>
</html>
